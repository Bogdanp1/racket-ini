MAKE            ?= make
RACKET          := racket
RACO            := raco
SCRIBBLE        := $(RACO) scribble

PWD             ?= $(shell pwd)
DOCS            := $(PWD)/docs
PUBLIC          := $(PWD)/public


src-%:
	$(MAKE) -C src $(*)


.PHONY: all
all: compile


.PHONY: clean
clean: src-clean

.PHONY: compile
compile: src-compile

.PHONY: install
install: src-install

.PHONY: setup
setup: src-setup

.PHONY: test
test: src-test

.PHONY: remove
remove: src-remove

$(PUBLIC):
	$(SCRIBBLE) ++main-xref-in --dest $(PWD) --dest-name public \
		--htmls --quiet $(DOCS)/scribblings/main.scrbl

.PHONY: public
public:
	if [ -d $(PUBLIC) ] ; then rm -r $(PUBLIC) ; fi
	$(MAKE) $(PUBLIC)
