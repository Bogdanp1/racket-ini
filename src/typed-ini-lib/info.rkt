#lang info


(define pkg-desc "Racket parser for Ini and UNIX Conf files. Typed interface.")

(define version "0.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "typed-racket-lib"
    "ini-lib"))

(define build-deps
  '())
