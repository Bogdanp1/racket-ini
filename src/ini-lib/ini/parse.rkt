;; This file is part of racket-ini - Racket parser for Ini and UNIX Conf files.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ini is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ini is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ini.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require "structs.rkt"
         (only-in racket/string string-trim))

(provide (all-defined-out))


(define (string* list-of-chars)
  (apply string list-of-chars))

(define (trim str)
  (string-trim str " "))


(define (tokenize [port (current-input-port)])
  (let loop ([tokens '()] [chars '()] [reading #false])
    (let ([char (read-char port)])
      (cond
        [(eof-object? char)
         (cond
           [reading
            `(,@tokens ,(cons reading (string* chars)))]
           [else
            tokens])]
        [(equal? char #\\)
         (loop tokens `(,@chars ,(read-char port)) reading)]
        [(or (equal? char #\;) (equal? char #\#))
         (read-line port)  ; discard to end of line
         (loop tokens '() #false)]
        [(equal? char #\newline)
         (case reading
           [(param-value)
            (loop `(,@tokens ,(cons reading (string* chars))) '() #false)]
           [else
            (loop tokens chars reading)])]
        [(equal? char #\])
         (case reading
           ;; When reading a section: finish that section.
           [(section)
            (loop `(,@tokens ,(cons reading (string* chars))) '() #false)]
           ;; When reading a parameter value: add to chars.
           [(param-value)
            (loop tokens `(,@chars ,char) reading)]
           ;; Else: ignore.
           [else
            (loop tokens chars reading)])]
        [(equal? char #\[)
         (case reading
           [(param-value)
            (loop tokens `(,@chars ,char) reading)]
           [(value)
            (loop `(,@tokens ,(cons reading (string* chars))) '() 'section)]
           [else
            (loop tokens chars 'section)])]
        [(equal? char #\space)
         (cond
           [reading
            (loop tokens `(,@chars ,char) reading)]
           [else
            (loop tokens chars reading)])]
        [(equal? char #\=)
         (case reading
           [(param-name)
            (loop `(,@tokens ,(cons reading (string* chars)))
                  '()
                  'param-value)]
           [else
            (loop tokens `(,@chars ,char) reading)])]
        [else
         (cond
           [reading
            (loop tokens `(,@chars ,char) reading)]
           [else
            (loop tokens `(,@chars ,char) 'param-name)])]))))


(define (translate-sections tokens)
  (let loop ([tokens tokens]
             [sections '()]
             [section-name '||]
             [section-params '()]
             [param-name #false])
    (cond
      [(null? tokens)
       (cond
         [section-name
          `(,@sections ,(section section-name section-params))]
         [else
          sections])]
      [else
       (let* ([token (car tokens)]
              [token-name (car token)]
              [token-value (cdr token)]
              [rest-tokens (cdr tokens)])
         (case token-name
           [(section)
            (loop rest-tokens
                  `(,@sections ,(section section-name section-params))
                  (string->symbol (string-trim token-value))
                  '()
                  #false)]
           [(param-name)
            (loop rest-tokens
                  sections
                  section-name
                  section-params
                  (string->symbol (string-trim token-value)))]
           [(param-value)
            (loop rest-tokens
                  sections
                  section-name
                  `(,@section-params ,(param param-name (trim token-value)))
                  #false)]))])))

(define (translate tokens)
  (ini (translate-sections tokens)))
