;; This file is part of racket-ini - Racket parser for Ini and UNIX Conf files.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ini is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ini is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ini.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require "structs.rkt"
         (only-in racket/port with-output-to-string))

(provide (all-defined-out))


(define (write-param p [output-port (current-output-port)])
  (fprintf output-port "~a = ~a\n" (param-name p) (or (param-value p) "")))

(define (write-section s [output-port (current-output-port)])
  (let ([s-name (section-name s)])
    (unless (equal? s-name '||)
      (fprintf output-port "[~a]\n" s-name)))
  (for ([p (section-params s)])
    (write-param p output-port)))

(define (write-ini i [output-port (current-output-port)])
  (for ([s (ini-sections i)])
    (write-section s output-port)))


(define (ini->string i)
  (with-output-to-string (lambda _ (write-ini i))))
