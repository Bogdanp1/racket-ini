;; This file is part of racket-ini - Racket parser for Ini and UNIX Conf files.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ini is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ini is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ini.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit ini)

  (define empty-toplevel
    (section '|| '()))

  (let ([empty-section (ini (list empty-toplevel (section 'asd '())))])
    (test-equal? "[asd]"
                 (string->ini "[asd]")
                 empty-section)
    (test-equal? "[[asd]]"
                 (string->ini "[[asd]]")
                 empty-section)
    (test-equal? "[[[asd]]]"
                 (string->ini "[[[asd]]]")
                 empty-section))

  (let ([section-A (section 'A (list (param 'a "1")))]
        [section-B (section 'B (list (param 'b "2")))]
        [section-C (section 'C (list (param 'c "3")))])
    (test-equal? "one inline section"
                 (string->ini "[A] a = 1")
                 (ini (list empty-toplevel section-A)))
    (test-equal? "two inline sections"
                 (string->ini "[A] a = 1\n[B] b = 2")
                 (ini (list empty-toplevel section-A section-B)))
    (test-equal? "three inline sections"
                 (string->ini "[A] a = 1\n[B] b = 2\n[C] c = 3")
                 (ini (list empty-toplevel section-A section-B section-C))))

  (test-equal? "three sections with three params"
               (string->ini "[A]
                             a1=1
                             a2=2
                             a3=3
                             [B]
                             b1=1
                             b2=2
                             b3=3
                             [C]
                             c1=1
                             c2=2
                             c3=3")
               (ini (list empty-toplevel
                          (section 'A (list (param 'a1 "1")
                                            (param 'a2 "2")
                                            (param 'a3 "3")))
                          (section 'B (list (param 'b1 "1")
                                            (param 'b2 "2")
                                            (param 'b3 "3")))
                          (section 'C (list (param 'c1 "1")
                                            (param 'c2 "2")
                                            (param 'c3 "3"))))))

  (test-equal? "noting" (string->ini "") (ini (list empty-toplevel)))

  (test-equal? "top level section parameters"
               (string->ini "a1 = 1\n a2 = 2")
               (ini (list (section '||
                                   (list (param 'a1 "1")
                                         (param 'a2 "2")))))))
