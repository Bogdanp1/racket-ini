;; This file is part of racket-ini - Racket parser for Ini and UNIX Conf files.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ini is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ini is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ini.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          ini
          (for-label racket
                     ini))


@(define example-eval
   (make-base-eval '(require ini)))


@title[#:tag "ini-reading"]{Reading Input Ini}

@defmodule[ini/read]

While reading there always will be a "empty" section with name @racket['||].

@examples[
 #:eval example-eval
 (section '|| '())
 ]

If there are parameters at so-called "top level" (without a section),
then they will be added to the "empty" section.

@examples[
 #:eval example-eval
 (section '|| (list (param 'paramA "valueA")))
 ]

@defproc[
 (read-ini [port input-port? (current-input-port)])
 ini?
 ]{
}

@defproc[
 (string->ini [str string?])
 ini?
 ]{

 @examples[
 #:eval example-eval
 (string->ini "paramA = valueA\n")
 (string->ini "paramA = valueA\n[config]\nparamB = valueB\n")
 (string->ini "[config]\nparamA = valueA\nparamB = valueB\n")
 ]
}

@defproc[
 (read-ini-file [port path-string?])
 ini?
 ]{
}
