;; This file is part of racket-ini - Racket parser for Ini and UNIX Conf files.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ini is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ini is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ini.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          ini
          (for-label racket
                     json
                     ini))


@(define example-eval
   (make-base-eval '(require ini json)))


@title[#:tag "ini-converting"]{Converting Ini}

@defmodule[ini/convert]

@defproc[
 (ini-section-params [i ini?] [section-name symbol?])
 (or/c (listof param?) #false)
 ]{

 @examples[
 #:eval example-eval
 (define my-ini
   (ini (list (section '|| (list (param 'paramA "valueA")))
              (section '|| (list (param 'paramB "valueB"))))))
 (ini-section-params my-ini '||)
 ]
}

@defproc[
 (ini->hash [i ini?])
 hash?
 ]{

 @examples[
 #:eval example-eval
 (define my-ini
   (ini (list (section 'configA (list (param 'paramA "valueA")
                                      (param 'paramB "valueB")))
              (section 'configB (list (param 'paramA "valueA")
                                      (param 'paramB "valueB"))))))
 (ini->hash my-ini)
 (jsexpr->string (ini->hash my-ini))
 ]
}

@defproc[
 (hash->ini [hsh hash?])
 ini?
 ]{

 @examples[
 #:eval example-eval
 (define my-hash
   (hash 'configA (hash 'paramA "valueA" 'paramB "valueB")
         'configB (hash 'paramA "valueA" 'paramB "valueB")))
 (hash->ini my-hash)
 (jsexpr->string my-hash)
 ]
}
